/* Katie Shaw, Nov 2017 */


$(document).ready(function() {   

// force a refresh
$(window).resize(function(){location.reload();});


// begin Mobile Slider
if ($(window).width() <= 991) {
  // init the counter
 countUp();

  // move the focus to the front
  var parent = $("#carousel");
  var childToMoveFirst = $(".panel:nth-child(2)", parent).remove();
  parent.prepend(childToMoveFirst);


  // display simple mobile slider
  $('.1').click(function() { showPanel(1); });
  $('.2').click(function() { showPanel(2); });
  $('.3').click(function() { showPanel(3); });
  // Note to self: Loop with a counter if more than 3 panels

  function showPanel(x){ 
    var activePanel = x;
    $('.panel').removeClass('left right focus');
    $('.panel:nth-child(' + activePanel + ')').addClass('focus');
    // now update the control buttons
    $('#control-mob span').removeClass('active');
    $('#control-mob span:nth-child(' + activePanel + ')').addClass('active');
    countUp();
  };

}

// Begin Desktop Slider
else {
  // init the counter
  countUp();

  // call the click functions
  $('.forward').click(function(){ carouselForward(); countUp(); });
  $('.back').click(function(){ carouselBack(); countUp(); });


  function carouselForward() {
    // animate based on classes 
    $('.left').animate({ left: '585px', opacity: '0.5' }, 400);
    $('.focus').animate({ left:'0px', opacity: '0.5', height: '467px', width: '372px', top: '90px'  }, 400);
    $('.right').animate({ left:'210px', opacity: '1', height: '621px', width: '537px', top: '0' }, 400);

    // remove old classes, add new ones
    $('.panel:nth-child(1)').removeClass('left').addClass('right');
    $('.panel:nth-child(2)').removeClass('focus').addClass('left');
    $('.panel:nth-child(3)').removeClass('right').addClass('focus');

    // now switch 
    var parent = $("#carousel");
    var childToMoveLast = $(".panel:nth-child(1)", parent).remove();
    parent.append(childToMoveLast);
  };


  function carouselBack() {
    // animate based on classes	
    $('.left').animate({ left:'210px', opacity: '1', height: '621px', width: '537px', top: '0' }, 400);
    $('.focus').animate({ left:'585px', opacity: '0.5', height: '467px', width: '372px', top: '90px' }, 400);
    $('.right').animate({ left:'0px', opacity: '0.5' }, 400);

    // remove old classes, add new ones
    $('.panel:nth-child(1)').removeClass('left').addClass('focus');
    $('.panel:nth-child(2)').removeClass('focus').addClass('right');
    $('.panel:nth-child(3)').removeClass('right').addClass('left');

    // now switch 
    var parent = $("#carousel");
    var childToMoveFirst = $(".panel:nth-child(3)", parent).remove();
    parent.prepend(childToMoveFirst);
  };
  // Note to self: Could be more concise, if time allows


};



// counter
function countUp() {
  var $helpers = "50892";

  // Fade out during transition
  $('#countup-wrapper').animate({ opacity: '0' }, 300);


  // Now fade back in when focused
  setTimeout( function() {
  $('.focus #countup-wrapper').animate({ opacity: '1' }, 300);

  // Animate up to the number of people helping
  $('.focus .countup .number').each(function () {
    $(this).prop('Counter', 0).animate({ Counter: ($helpers)}, { duration: 700, easing: 'swing', step: function(now)
      { $(this).text(Math.ceil(now).toLocaleString('en')); }
    });
  });
 }, 400);

};



}); 


