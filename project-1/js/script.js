/* Katie Shaw, Nov 2017 */


$(document).ready(function() {   

// force a refresh
$(window).resize(function(){location.reload();});



if ($(window).width() >= 768) { $('#kit .btn').html('GET YOUR FREE STARTER KIT'); }
else { $('#kit .btn').html('GET YOUR FREE KIT'); }


// popup video on cta click
$('#cta').click(function() {
	$('#overlay').css('display', 'block');
	$('#video-popup').css('display', 'block');
	$('#overlay').animate({'opacity': '0.99'}, 2000);
	$('#video-embed').animate({'opacity': '1'}, 6000);

	var iframe = $('#player1')[0];
    var player = $f(iframe);
    player.api('play');
});





// close video
$(function() {
	var iframe = $('#video-embed iframe')[0];
	player = $f(iframe);
	player.addEvent('ready', function() {
	player.addEvent('finish', onFinish);
	});
});
	

function onFinish(id) {
	$('#video-popup').css('display', 'none');
	$('#overlay').css('display', 'none');

	// scroll to relevant content and animate into view
	$('html, body').animate({ scrollTop: $("#kit-wrapper").offset().top }, 500); 
	$('#kit > img').css({'bottom': '-225px'});
	$('#kit > p').css({'opacity': '0'});
	$('#kit .btn').css({'opacity': '0'});

	$('#kit > p').animate({'opacity': '1'}, 2000);
	$('#kit > img').animate({'bottom': '0'}, 1000);
	$('#kit .btn').animate({'opacity': '1'}, 2000);
};









});
